\documentclass[a4paper,11pt]{article}

%\usepackage[showframe]{geometry} %use this if you want to check margins
\usepackage{geometry}
\geometry{centering,total={160mm,250mm},includeheadfoot}

%auxiliairy files
\input{header.tex} %usepackage and configurations
\input{cmds.tex} %user defined commands

%title
\newcommand{\doctitle}{Security (SECI5-SECG4)}
\newcommand{\docsubtitle}{Frequential analysis}
\newcommand{\tdyear}{2022 - 2023}
\author{R. Absil, N. Richard}

%language input
\lstset{language=c++,
		morekeywords={constexpr,nullptr}}

%language={[x86masm]Assembler}
%language=c++, morekeywords={constexpr,nullptr}
%language=Java

\newcommand{\deadline}{Sunday 16 April at 23h59\xspace}
\newcommand{\deadlinesubscription}{Friday 31 March at 23h59\xspace}

\begin{document}
\pagestyle{fancy} %displays custom headers and footers

\maketitle

The goal of this homework is to make you implement the cryptanalysis of the Cesar and Vernam ciphers, with frequential analysis. You have to submit your work on \deadline at the latest. You can work in pairs (groups of 2 students).

\section{The cipher of Vernam}

The cipher of Vernam is a simple cipher algorithm. Given a key of $n$ bytes $k_1,k_2,\dots,k_n$,
\begin{itemize}
\item we cipher byte $p_i$ of a plain text of $n$ bytes $p_1,p_2,\dots,p_n$ by computing the bitwise-xor of $p_i$ and $k_i$. For instance, if $p_3 = 01001000$ and $k_3 = 01010000$, the third output ciphered byte is $01001000 \oplus 01010000 = 00011000$;
\item we uncipher byte $c_i$ of a ciphered text of $n$ bytes $c_1,c_2,\dots,c_n$ by computing the bitwise-xor of $c_i$ and $k_i$. For instance, if $c_3 = 00011000$ and $k_3 = 01010000$, the third output unciphered byte is $00011000 \oplus 01010000 = 01001000$.
\end{itemize}
Remark that the length of the key is \emph{exactly} the length of the plain/ciphered text.

The Vernam cipher is a famous for the following result, stated by Paar and Pelz~\cite{crypto}:
\begin{quote}
A cryptosystem is unconditionally or information-theoretically secure if it cannot be broken even with infinite computational resources.
\end{quote}
When properly used, that is, when the key is a securely randomly generated nounce at least as large as the plain text, the cipher of Vernam has such property.

Note that this condition is unpractical: with every transaction, we need to securely generate a new key with appropriate length. As a matter of fact, some contries failed to use it properly in times of need, with severe consequences\footnote{\url{https://en.wikipedia.org/wiki/Venona_project} - Last accessed on Oct., 6, 2022.}.

In this lab, we will deliberately use the cipher wrong, and consequently break it. We will shorten the key to a length vastly shorter than that of the plain text\footnote{Note that this is equivalent to reusing the key for another text.}. To perform cipher operations, we ``concatenate'' the key to itself enough times so it reaches at least the end of the plain text, and then perform bitwise-xor, in a similar way to what the cipher of Vigenere does with some short key and alphabet shifts.

For instance, if the plain text is $01001000$ and the key is $01$, the ciphered text is $01001000 \oplus \underline{01}\,\underline{01}\,\underline{01}\,\underline{01} = 00011101$.

\section{Objectives}

You must, from a sufficiently large text file, implement\footnote{the choice of programming language is left to your discretion (you take responsibility for this choice).} an \emph{automatic} cryptanalysis of the Cesar and Vernam ciphers\footnote{... with the above wrong use, that is, with a non-nounce key shorter than the input text.}. For that purpose, you must use a frequential analysis attack, as described in the course (or any other method you see fit). Consequently, your code must allow to
\begin{itemize}
  \item encode and decode text (plain or not) using these ciphers with the help of a key,
  \item decode ciphered text (ciphered with any of these algorithms) \emph{without} the key.
\end{itemize}
Note that the vast majority of the mark you will get is related to that second objective.

%Note that in both cases, it is possible that the "e" is not the most frequent letter in the plain text. Consequently, it is probably useful that your application consider this case automatically.%\footnote{For that purpose, you will probably have to implement concepts relative to statistical analysis, namely the coincidence index, and maybe the $\chi^2$ test ("Chi 2 test"). Note that you don't have to \emph{master} these concepts: just know when to use them and why, and correctly implement them.}.

Finally, considering this howework is very short and basically consists in coding scripts rather and implementing a ``true'' project, you don't have to document your code more than what the minimum requires, or submit a report.

\section{Text and preprocessing}

You can find large texts on Gutenberg.org\footnote{\url{https://www.gutenberg.org/browse/languages/en} - Last accessed on Oct., 6, 2022.}. Note that, for simplicity reasons, you are allowed to preprocess your plain texts, for instance to delete spaces\footnote{Whatever space it is, such as regular space, tabulation, etc.} and diacritics, set all letters to lower case, etc.

The ciphered text also doesn't need to be under a ``human readable format'': contrary to a Cesar cipher, the ciphered text will not be in the range $[A,Z]$. You can leave the xored bytes as they are.

\section{Submission modalities}

Projects can be implemented in pairs (groups of 2 students), and submitted with the help of a git\footnote{The only instances accepted are \url{https://gitlab.com} and \url{https://git.esi-bru.be}.} repository\footnote{Create the repository yourself, add your teacher as maintainer.}. For that purpose, send an email\footnote{The automatic notification is \emph{not enough}.} to your teacher on \deadlinesubscription at the latest with the SSH URL\footnote{A git SSH URL looks like \texttt{git@git.esi-bru.be:username/projectname}.} to your repository, and the name and matricule of your group members\footnote{I love footnotes.}.

You have to submit your work on \deadline at the latest. The minimal requirements for submitted projects are as follows:
\begin{itemize}
\item projects have to be submitted on time\footnote{I retrieve your projects \emph{on time} with the command \texttt{git pull origin main}. In no way shall I grade or even look at another branch.},
\item projects have to provide a readme file
	\begin{itemize}
	\item mentioning the name and matricule of your group members,
	\item explaining how to build your project\footnote{Projects that do not compile will not be graded.} (we recommend here to either provide a makefile, or a shell script to install missing dependencies, compile the project and run relevant scripts),
	\item explaining how to use your project (for example, ``to run the cryptanalysis of Vernam, run the following command in a shell'').
	\end{itemize}
\end{itemize}

Projects failing to meet these requirements will not be graded (that is, they will get 0/20). Furthermore, note that we shall in \emph{no way} build or run your projects in an IDE.

\bibliography{biblio}
\bibliographystyle{abbrv}

\end{document}

