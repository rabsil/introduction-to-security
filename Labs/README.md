# Introduction to security - Romain Absil (rabsil@he2b.be)

## Build and clean

Note that you need an updated full LaTeX distribution (such as the one provided by texlive) as well as latexmk to build the slides.


### Labs

For each laboratory (located in the "Labs" folder), 
- briefs can be built using the `make` command;
- `make clean` will clear the built files;

### Projects
For each project (located in the "projects" folder), 
- briefs can be built using the `make` command;
- `make clean` will clear the built files;

## License

This work is under Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0).

Full legal code can be found on [Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/legalcode)
