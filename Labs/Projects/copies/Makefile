BUILD=build
LATEX=latexmk -recorder -pdf -pdflatex="pdflatex --shell-escape %O %S" \
		-outdir=$(BUILD)
MAIN=copies
SUBS=.pdf
ALLPDF=$(addprefix $(MAIN), $(SUBS))
PREVIEWS=$(addsuffix .preview, $(ALLPDF))

.PHONY: all latexmkbuild $(PREVIEWS)

all: $(ALLPDF)

$(PREVIEWS): %.preview: latexmkbuild
	$(LATEX) -pvc -interaction=nonstopmode $(patsubst %.pdf, %.tex, $*)

$(ALLPDF): %.pdf: latexmkbuild
	$(LATEX) $(patsubst %.pdf, %.tex, $@)
	ln -fs $(BUILD)/$@ .

clean:
	rm -rf $(BUILD)
	rm -f $(ALLPDF)

# This recipe is always made (because it is phony). Because we need to rely on
# latexmk to check for file dependencies.
latexmkbuild:
	mkdir -p $(BUILD)
