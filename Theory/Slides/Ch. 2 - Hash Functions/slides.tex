\input{../common/header.tex}
\input{../common/cmds.tex}

\title{Ch. 2 - Hash Functions}

\begin{document}
\input{../common/front.tex}

\section{Introduction}

\begin{frame}
\frametitle{Basic principles}
\begin{itemize}[<+->]
\item Hash functions with additional security-related characteristics
\item From a message, a hash function outputs a \emph{message digest} (hash)
\item A hash is simply a numerical imprint of a bloc of datas
\item Two different messages have different hashes
\item It is hard to recover a message from its hash
\item It is hard to find collisions
\item Other characteristics
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Applications  :  "encrypted" password}
\begin{itemize}[<+->]
\item A user create an account (login,password)
\item The system has a hashtable indexed by logins
\item We don't want to cipher the table, or each entry separately
\item Instead of storing a password, we store its hash
	\begin{itemize}
	\item Hash function $f$
	\end{itemize}
\item Looking at the table is useless
\item When the system gets an authentication request \texttt{(login,password)}, we check if $t\left[login\right]=hash(password)$
	\begin{itemize}
	\item If it is, we grant access
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Application : data integrity}
\begin{exampleblock}<+->{Academic example}
	\begin{itemize}[<+->]
	\item A billionaire writes his will
	\item "When I die, my wealth will be equally shared between my three sons. However, George will receive nothing"
	\item The father leaves the testament at some notary, and gives the hash to another one
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item If Georges corrupts / tortures the notary who has the message, falsification is detected with the hash
\item It is also impossible to find a message having the same hash as the one the second notary has
\end{itemize}
\end{frame}

\section{Characteristics}

\begin{frame}
\frametitle{Characteristics}
\begin{itemize}[<+->]
\item Let $f : M \mapsto \IN$, $f$ is a cryptographic hash function if
	\begin{enumerate}
	\item computing $f(x)$ is easy for each $x\in M$
	\item $f$ is resistant to pre-image
		\begin{itemize}
		\item it is hard to invert $f$,	
		\end{itemize}
	\item $f$ is resistant to second pre-image
		\begin{itemize}
		\item it is hard to change a message without its hash
		\end{itemize}
	\item $f$ is resistant to collisions
		\begin{itemize}
		\item it is hard to generate two different messages with the same hash
		\end{itemize}
	\end{enumerate}
\item It is currently unknow if a function having the first two characteristics actually exists
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Good hash functions}
\begin{itemize}[<+->]
\item In practice, we require several other characteristics
	\begin{itemize}
	\item The hash function must be public (no key, no secret)
	\item The output must have a fixed size, whatever the size of the input
		\begin{itemize}
		\item Makes pratical uses easy
		\item Makes you vulnerable to birthday attacks
		\item Based on the Merkle-Damgård construction and one-way compression functions
		\end{itemize}
	\item Images are uniformly distributed
		\begin{itemize}
		\item The hash function "suffles the input well"
		\end{itemize}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Bad hash functions}
\begin{itemize}[<+->]
\item Add ASCII values of the characters of a message, modulo~$n$
	\begin{itemize}
	\item Collisions problems, not resistant ro second pre-image
	\end{itemize}
\item Concatenate ASCII values of the characters of a message, modulo~$n$
	\begin{itemize}
	\item Not resistant to pre-image
	\end{itemize}
\end{itemize}
\end{frame}

\section{Building up hash functions}

\begin{frame}
\frametitle{One-way compression function}
\begin{exampleblock}<1->{Definition}
	\begin{itemize}[<2->]
	\item Function mapping two fixed-size inputs to a (smaller) fixed-size output
	\end{itemize}
\end{exampleblock}
\begin{minipage}{0.8\textwidth}
\begin{itemize}
\onslide<4-> \item Must have the requirements of cryptographic hash funcions
	\begin{enumerate}
	\onslide<5-> \item Easy to compute
	\onslide<6-> \item Pre-image resistant
	\onslide<7-> \item Second pre-image resistant
	\onslide<8-> \item Collision resistant
		\begin{itemize}
		\onslide<9-> \item Warning : birthday attack
		\end{itemize}
	\end{enumerate}
\onslide<10-> \item Unrelated to data compression (can be reverted)
\end{itemize}
\end{minipage}
\begin{minipage}{.175\textwidth}
\visible<3->{\includegraphics[width=\textwidth]{pics/compr.pdf}}
\end{minipage}
\end{frame}

\begin{frame}
\frametitle{Merkle-Damg\aa rd (1/3) construction}
\begin{itemize}[<+->]
\item We want to build hash functions with desired characteristics
\item They must be able to process a message with arbitrary size into a fixed-size hash
\item Principle : build up hash functions from one-way compression functions
\item Elaborated independently by Ralph Merkle and Ivan Damg\aa rd in 1979
\end{itemize}
\begin{exampleblock}<+->{Theorem}
	\begin{itemize}[<+->]
	\item If an appropriate padding scheme is used and the one-way compression function is collision-resistant, then the built hash function is collision-resistant
	\end{itemize}
\end{exampleblock}
\end{frame}

\definecolor{darkred}{RGB}{180,0,0}
\definecolor{dandelion}{rgb}{0.94, 0.88, 0.19}

\begin{frame}
\frametitle{Merkle-Damg\aa rd (2/3) construction}
\begin{enumerate}
\onslide<1->{\item Slice the message into blocks of fixed size}
\onslide<5->{\item The last block is filled with arbitrary (\emph{i.e., fixed ones}) values to get the required length}
\onslide<7->{\item We set an initialisation vector (IV)}
\onslide<9->{\item On the first iteration, we input IV and the first block to $f$}
\onslide<11->{\item In the general case, we input the current result along with block $k$ to $f$}
\onslide<15->{\item Possible use of a finalisation function (shuffling, avalanching, etc.)}
\end{enumerate}
\begin{center}
%\includegraphics[width=\textwidth]{pics/merkle.pdf}
\scalebox{.4}{
\begin{tikzpicture}
\onslide<8->{\node[draw, circle, minimum size = 40pt, fill=blue!30] (iv) at (0,0) {\LARGE \tt IV};}

\onslide<10->{\node[draw, minimum size = 40pt, fill=darkred] (f1) at (4,0) {\LARGE $f$};}
\onslide<12->{\node[draw, minimum size = 40pt, fill=darkred] (f2) at (8,0) {\LARGE $f$};}
\onslide<13->{\node[draw, minimum size = 40pt, fill=darkred] (f3) at (15,0) {\LARGE $f$};}
\onslide<14->{\node[draw, minimum size = 40pt, fill=darkred] (f4) at (19,0) {\LARGE $f$};}

\onslide<16->{\node[draw, minimum size = 40pt, fill=blue!30] (fin) at (24,0) {\tt Finalisation};}
\onslide<16->{\node[draw, circle, minimum size = 40pt, fill=blue!30] (out) at (28,0) {Hash};}

\onslide<2->{\node[draw, minimum height=40pt, minimum width=114pt, fill=dandelion!80] (b1) at (4, 3) {Bloc $1$};}
\onslide<3->{\node[draw, minimum height=40pt, minimum width=114pt, fill=dandelion!80] (b2) at (8, 3) {Bloc $2$};}
\onslide<4->{\node[draw, minimum height=40pt, minimum width=114pt, fill=dandelion!80] (b3) at (15, 3) {Bloc $n-1$};}
\onslide<6->{\node[draw, minimum height=40pt, minimum width=114pt, fill=dandelion!80] (b4) at (19, 3) {Bloc $n$ + padding};}

\node at (11.5,3) {\dots};

\onslide<10->{\draw[_arc, very thick] (iv) -- (f1);}
\onslide<12->{\draw[_arc, very thick] (f1) -- (f2);}
\onslide<13->{\draw[_arc, very thick, dotted] (f2) -- (f3);}
\onslide<14->{\draw[_arc, very thick] (f3) -- (f4);}
\onslide<16->{\draw[_arc, very thick] (f4) -- (fin);}
\onslide<16->{\draw[_arc, very thick] (fin) -- (out);}

\onslide<10->{\draw[_arc, very thick] (b1) -- (f1);}
\onslide<12->{\draw[_arc, very thick] (b2) -- (f2);}
\onslide<13->{\draw[_arc, very thick] (b3) -- (f3);}
\onslide<14->{\draw[_arc, very thick] (b4) -- (f4);}
\end{tikzpicture}
}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Merkle-Damg\aa rd construction (3/3)}
\begin{itemize}[<+->]
\item Very popular in practice
\item Nevertheless, has undesired effects
	\begin{enumerate}
	\item if an attacker has a collision, he can find other ones easily,
	\item second pre-image attacks are more efficient than brute force
	\end{enumerate}
\item Improved by enforcing that the size of a block is stricly longer than the size of the output
	\begin{itemize}
	\item Wide-pipe construction : if $n$ bits are desired, a bloc is of size $2n$ and the finalisation function reduces the size of the output
	\item Simple example : truncate
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Birthday attack (1/2)}
\begin{itemize}
\onslide<1-> \item Attack exploiting the "birthday paradox" (probability theory)
\end{itemize}
\onslide<2-> 
\begin{exampleblock}{Problem}
	\begin{itemize}
	\onslide<3->\item In a set of $n$ people, what is the probability that two of them have the same birthday?
	\end{itemize}
\end{exampleblock}
\begin{itemize}
\onslide<4-> \item Famous probability approximation problem
\onslide<5-> \item By the pigeonhole principle, this probability is zero when $n=1$ and $1$ when $n=367$.
\end{itemize}
\onslide<6-> 
\begin{center}
\begin{tiny}
\begin{tabular}{|c|cccccccccccc|}
\hline
\textbf{n} & 1 & 5 & 10 & 20 & \emph{23} & 30 & 40 & 50 & 60 & 70 & 100 & 367\\
\hline
\textbf{p(n)} & 0 & 0.027 & 0.117 & 0.417 & 0.507 & 0.706 & 0.891 & 0.97 & 0.994 & 0.999 & 0.9999997 & 1\\
\hline
\end{tabular}
\end{tiny}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Birthday attack (2/2)}
\begin{itemize}[<+->]
\item This theorem is really important regarding cryptographic hash functions
\end{itemize}
\begin{alertblock}<+->{Collision bound}
	\begin{itemize}[<+->]
	\item If a hash function has $n$ possible output values, $\sqrt{n}$ computations are enough to have a probable ($\simeq$ 50\%) collision
	\end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
	\item In a set of $23 \simeq \sqrt{365}$ people, it is likely that two people have the same birthday
\end{itemize}
\end{frame}

%\section{\java\ Implementation}
%
%\begin{frame}
%\frametitle{Generalities}
%\begin{itemize}[<+->]
%\item Package \texttt{java.security}
%\item Class \texttt{MessageDigest} : superclasse des algorithmes de hachage.
%\item Trois méthodes utilisées principalement : 
%\begin{itemize}[<+->]
%	\item \texttt{getInstance( ... )}
%	\item \texttt{update( ... )}
%	\item \texttt{digest()}
%\end{itemize}
%\end{itemize}	
%\end{frame}
% 
%\begin{frame}[containsverbatim]
%\frametitle{Exemple}
%\begin{itemize}
%\item Simple example on how to compute a SHA-1 hash from a string
%\end{itemize}
%\begin{lstlisting}
%MessageDigest alg = MessageDigest.getInstance("SHA-1");
%byte[] bytes = "Message".getBytes();
%alg.update(bytes);
%byte[] hash = alg.digest();
%\end{lstlisting}
%\end{frame}
% 
%\begin{frame}[containsverbatim]
%\frametitle{Generic hash}
%\begin{lstlisting}
%public byte[] hash(InputStream in, MessageDigest alg, int bufferSize) 
%	throws IOException
%{
%	alg.reset();
%	byte[] buffer = new byte[bufferSize];
%	int readLength = 0;
%	boolean done = false;
%	while(!done)
%	{
%		readLength = in.read(buffer);
%		alg.update(buffer,0, readLength);
%		if(readLength != buffer.length)
%			done = true;
%	}
%	return alg.digest();
%}
%\end{lstlisting}
%\end{frame}
% 
%\begin{frame}
%\frametitle{Remarks}
%\begin{itemize}[<+->]
%\item Hash algorithms supported by jdk 1.9 (non exhaustive) :  \texttt{"SHA-1"}, \texttt{"SHA-256"}, \texttt{"SHA-384"}, \texttt{"SHA-512"}, \texttt{"MD2"}, \texttt{"MD5"}, ...
%\item Check exceptions : \texttt{NoSuchAlgorithmException}, \texttt{NoSuchProviderException}, ...
%\item Choose your hash function depending on your needs
%	\begin{itemize}
%	\item Passwords
%	\item (Non)sensitive integrity
%	\end{itemize}
%\item Some hash functions are dedicated to passwords
%\end{itemize}
% \end{frame}

\end{document}
