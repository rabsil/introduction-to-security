import arithm

wrong = ''
for i in range(1, 30):
    if i < 10:
        wrong += str(i) + '  : '
    else:
        wrong += str(i) + ' : '
    for j in range(i, 30):
        bez = arithm.bezout(i, j)
        if bez[0] != i * bez[1] + j * bez[2]:
            if j < 10:
                wrong += str(j) + '  '
            else:
                wrong += str(j) + ' '
        else:
            wrong += '   '
            
    wrong += '\n'

print(wrong)