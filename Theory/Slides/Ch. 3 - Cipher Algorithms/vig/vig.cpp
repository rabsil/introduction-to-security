#include <iostream>
#include <string>
#include <vector>

using namespace std;

string cipher(string plain, vector<int> key)
{
	static char alpha[26] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

	string cipher;
	for(int i = 0, j = 0; i < plain.size(); i++)//i : index in text, j : index in key
	{
		int k = 0; //search index in alpha
		bool space = plain[i] == ' ';
		while(k < 26 && !space)
		{
			if(plain[i] == alpha[k])
				break;
			k++;
		}
		if(k == 26)
		{
			cerr << "ERROR" << endl;
			break;
		}

		else
		{
			if(space)
			{
				cipher += ' ';
				continue;
			}

			cipher += alpha[(k + key[j] - 1) % 26];
			j = (j + 1) % key.size();
		} 	
	}

	return cipher;
}

int main()
{
	cout << cipher("SECURITY COURSE",{9,14,6,15}) << endl;
}
