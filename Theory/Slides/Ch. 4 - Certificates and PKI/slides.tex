\input{../common/header.tex}
\input{../common/cmds.tex}

\title{Ch. 4 - Certificates and PKIs}

\begin{document}
\input{../common/front.tex}

\section{Introduction}

\begin{frame}
\frametitle{Huston, we have a problem. What is it?}
\begin{itemize}[<+->]
\item Consider the context of encrypting data
\end{itemize}
\begin{exampleblock}<+->{Scenario}
    \begin{itemize}[<+->]
    \item Alice wants to send a message that only Oscar can read
    \item Alice ciphers the message with the public key of Oscar
    \item Alice sends the message to Oscar
    \end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Only the owner of the private key matching the public one that was used can uncipher the message
\end{itemize}
\begin{alertblock}<+->{Problem}
    \begin{itemize}[<+->]
    \item Anyone could have generated a pair of keys
    \item We don't know if the owner is actually Oscar
    \end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{Nothing. Please, tell me.}
\begin{itemize}[<+->]
\item Consider the context of signing data
\end{itemize}
\begin{exampleblock}<+->{Scenario}
    \begin{itemize}[<+->]
    \item Alice wants to make sure some message comes from Oscar
    \item Oscar signs the message with his private key
    \item Oscar sends the message to Alice
    \end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Only the owner of the private key matching the public one that was used to authenticate could have signed
\end{itemize}
\begin{alertblock}<+->{Problem}
    \begin{itemize}[<+->]
    \item Anyone could have generated a pair of keys
    \item We don't know if the owner is actually Oscar
    \end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{No. If you cared, you would know what the problem is.}
\begin{itemize}[<+->]
\item In both previous examples, we don't actually know who is Oscar
    \begin{itemize}
    \item Anyone could have generated a pair of keys
    \item Anyone can pretend to be Oscar
    \end{itemize}
\item We need to link public keys to their owners
\item Some verification of the actual identify of the owner must be made
\item Because of the scale of the system, centralisation cannot be used
\item Some form of consensus must be made
\end{itemize}
\begin{exampleblock}<+->{Basic consensus rules}
    \begin{itemize}
%    \item The actors of the consensus set up a set of rules that they agree to follow
    \item The actors of the consensus decide who among them can be trusted
    \item The actors of the consensus have to be trusted by outsiders
    \end{itemize}
\end{exampleblock}
\end{frame}

\section{Certificate and PKI}

\begin{frame}
\frametitle{Principle}
\begin{itemize}[<+->]
\item Alice and Oscar have a common relation that they both trust
\item Oscar meets this trusted third party intermediate and gives his public key
\item The intermediate recognises Oscar and guarantees his identity
\item The intermediate uses his own private key to sign the public key of Oscar, and sends it to Alice
	\begin{itemize}
	\item Alice can check that the private key has not been altered
	\item Alice can check that the key has been signed by the trusted intermediate.
	\end{itemize}
\item Alice can authenticate the message without risks
\item In practice, a third party trusted intermediate is called a \emph{certification authority}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Examples of certification authorities}
\begin{small}
\begin{center}
\begin{tabular}{|l|c|c|}
\hline
\textbf{Nom} & \textbf{Use} & \textbf{Market share}\\
\hline\hline
Identrust & 46.5\% & 52.3\% \\
Digicert & 16\% & 18\% \\
Sectigo & 13.5\% & 15.2\% \\
GoDaddy & 5.4\% & 6.1\% \\
Let's Encrypt & 5.2\% & 5.8\% \\
GlobalSign & 2.6\% & 2.9\% \\
Certum & 0.6\% & 0.6\% \\
\hline
\end{tabular}
\end{center}
\end{small}
\begin{itemize}
\item Source: \emph{Usage of SSL certificate authorities for websites}, \url{http://w3techs.com/technologies/overview/ssl_certificate/all} - 22/09/2022.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Certificates and PKI}
\begin{exampleblock}<+->{Terminology}
	\begin{itemize}[<+->]
	\item \emph{(Public key) certificate} : electronic document used to prove the property of a public key
	\item \emph{Public key infrastructure (PKI)} : set of hardware, software, people, policies and protocols needed to create, manage, distribute, use, store and revoke certificates
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Goal of a certificate: proves the ownership a public key
\item Goal of a PKI
	\begin{enumerate}
	\item Make information transfer easier in network services (e-trade, etc.)
	\item Link public keys to their owners with a certification authority
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{X.509 certificates}
\begin{itemize}[<+->]
\item PKI standard
\item Specifies, among others, the format of public key certificates, their revocation, validation paths, etc.
\item Structure of a X.509 v3 certificate :
	\begin{itemize}
	\item Version number
	\item Serial number
	\item Signature algorithm
	\item Emitter
	\item Validity period
	\item Name of the client
	\item Information related to the public key of the client
		\begin{itemize}
		\item Algorithm
		\item Public key
		\end{itemize}
	\item Additional information
	\item Algorithm used to sign the certificate
	\item Signature of the certificate
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Self-signed certificates}
\begin{itemize}[<+->]
\item Certificated issued by the owner of the embedded public key
    \begin{itemize}
    \item No certification autority issued them
    \end{itemize}
\end{itemize}
\begin{alertblock}<+->{Problem}
    \begin{itemize}[<+->]
    \item Who are you?
    \item The King of England
    \item Who garantees it?
    \item Me, the King of England
    \end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item If a website delivers a self-signed certificate, we cannot check wether an attacker replaced it with his own
    \begin{itemize}
    \item Acceptable during development, and for root certificates (cf. Chain of Trust)
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Certificate signing request}
\begin{itemize}[<+->]
\item Most of the times, a CA does not fully generate a certificate
    \begin{itemize}
    \item If they did, they generated a keypair, and consequently have a copy of the private key
    \end{itemize}
\item The keypair is generated on client side
\item The public key is embedded in a partial certificate, to be signed by the CA
    \begin{itemize}
    \item Certificate signing request (CSR)
    \end{itemize}
\item The CSR contains signaletic information, the public key, information about the cipher, and is signed
\item It is send to the CA, who
    \begin{enumerate}
    \item checks the signature (proves integrity, and that you have the private key)
    \item signs it (further information can be required and identity checks can be made)
    \end{enumerate}
\item You then have a full certificate
\end{itemize}
\end{frame}

\subsection{HTTPS}

\begin{frame}
\frametitle{HTTPS protocol}
\begin{itemize}[<+->]
\item When you connect to a server, you want
    \begin{itemize}
    \item to actually connect to the actual server,
    \item the privacy and integrity of exchanged data.
    \end{itemize}
\item Extension of HTTP, over TLS
\item Prevents MITM attacks, replay attacks, etc.
\item Bidirectional encryption between client and server
\item Enabled with certificates
\item The first step of the connection is to perform a TLS handshake
\end{itemize}
\begin{exampleblock}<+->{Objective}
    \begin{itemize}[<+->]
    \item Securely generate and share a session key using DHKE
    \item That key will be used to encrypt data in both ways
    \end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}
\frametitle{TLS 1.3 handshake (1/4)}
\begin{itemize}
\item Main objective: share a symmetric key for encryption
\item Shorter than previous versions
\item Depreciates cipher suites considered unsafe
    \begin{itemize}
    \item No support for the RSA key exchange, for instance
    \end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Step 1: Client Hello}
    \begin{enumerate}[<+->]
    \item The client generates a random key pair
    \item The client sends a list of supported ciphers, a random nounce, and the generated public key
    \end{enumerate}
\end{exampleblock}
\begin{itemize}[<+->]
\item Assumes the client knows the preferred cipher of the server
    \begin{itemize}
    \item Considerably reduces the length of exchanges (as in TLS 1.2)
    \end{itemize}
\item If it fails, further steps will be needed
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{TLS 1.3 handshake (2/4)}
\begin{itemize}[<+->]
\item The server receives the Client Hello message
\end{itemize}
\begin{exampleblock}<+->{Step 2: Server Hello}
    \begin{enumerate}[<+->]
    \item If the server supports the algorithm the client guessed, it generates a key pair
    \item In that case, it sends the public key it generated and a random nounce (both signed), and its certificate
    \end{enumerate}
\end{exampleblock}
\begin{itemize}[<+->]
\item If the server does not support it, another Client Hello message is sent
    \begin{itemize}
    \item With a correct public key
    \end{itemize}
\item Both Client Hello and Server Hello are sent as plain text
\item Key pairs are trashed as soon as the handshake finishes
    \begin{itemize}
    \item Enables forward secrecy
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{TLS 1.3 handshake (3/4)}
\begin{itemize}[<+->]
\item Both client and server now agree on the cipher suite to use
\end{itemize}
\begin{exampleblock}<+->{Step 3: Client Finish}
    \begin{enumerate}[<+->]
    \item The client checks the certificate with its CA, and the signature
    \item The client generates a pre-master secret, encrypt it with the server public key, and sends it
    \item The client uses DH to generate the master secret, with the pre-master secret and its public key
    \end{enumerate}
\end{exampleblock}
\begin{itemize}[<+->]
\item Finally, the client generates a session key with a key derivation algorithm from the master secret
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{TLS 1.3 handshake (4/4)}
\begin{exampleblock}<+->{Step 4: Server Finish}
    \begin{enumerate}[<+->]
    \item The server receives the pre-master secret from the client, and decrypts it
    \item The server uses DH to generate the master secret, with the pre-master secret and its public key
    \end{enumerate}
\end{exampleblock}
\begin{itemize}[<+->]
\item Finally, the server generate a session key with a key derivation algorithm from the master secret
\item Now, both the client and the server have a session key (the same) that they can use for symmetric encryption
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Concluding remarks}
\begin{itemize}[<+->]
\item The nounces are used as IV for the cipher algorithm
    \begin{itemize}
    \item Depends on the chosen cipher suite
    \end{itemize}
\item We assumed that DHKE was used
    \begin{itemize}
    \item Other schemes exist
    \end{itemize}
\item Once we are ready to send data, we don't "simply" cipher it
    \begin{itemize}
    \item Because it can be replayed, and tampered with
    \end{itemize}
\item Standard protection: AEAD
    \begin{itemize}
    \item Authenticated encryption with associated data
    \item Checks both integrity of encrypted and unencrypted data
    \item Standard implementation: MtE
    \end{itemize}
\end{itemize}
\end{frame}

\section{Chain of trust}

\begin{frame}
\frametitle{Who can we trust?}
\begin{exampleblock}<+->{How can we trust a CA?}
    \begin{itemize}[<+->]
    \item Consensus
    \end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item A consensus of CA decides which CA to trust, and which cannot
\item "Large" software companies also decide which CA can be trusted
    \begin{itemize}[<+->]
    \item Microsoft, Apple, Linux foundation, Mozilla, Google, etc.
    \item Companies that provide browsers or secure authentication
    \end{itemize}
\item Good faith prevails
    \begin{itemize}
    \item If a CA misbehaves, others CA disavow it
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Examples}
\begin{itemize}[<+->]
\item Security breach
    \begin{itemize}
    \item The CA is hacked, and issues fraudulent certificates
    \item Allows hackers to use fraudulent certificates to MitM domains
    \item Historical example: DigiNotar in 2011
    \end{itemize}
\item Deliberate behaviour
    \begin{itemize}
    \item If a governement controls a CA, it can force it to issue fraudulent certificates
    \item Allows a governement to MitM domains
    \item Historical example: China Internet Network Information Center in 2009 and 2015
    \end{itemize}
\item Basic defense: takeover, dismantle, untrust such CA
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Chain of trust}
\begin{itemize}[<+->]
\item If is difficult, on practice, to have only a few CA
    \begin{itemize}
    \item Lots of different CA, with different price ranges, different requirements, etc.
    \end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Question}
    \begin{itemize}[<+->]
    \item How can a certificate issued by some CA recognised by Apple can be trusted on somebody running a Microsoft software?
    \end{itemize}
\end{exampleblock}
\begin{block}<+->{Answer}
    \begin{itemize}[<+->]
    \item Apple and Microsoft and a third party intermediate that they both trust
    \end{itemize}
\end{block}
\begin{itemize}[<+->]
\item That CA signs both Microsoft and Apple certificates
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Illustration}
\begin{center}
\includegraphics[width=.9\textwidth]{pics/Chain_Of_Trust.png}
\end{center}
\begin{itemize}
\item Source: public domain
\item Example: issuing of CST's
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Root certificate}
\begin{itemize}[<+->]
\item CA issue certificates in the form a a tree structure
\item The highest certificate in a chain of trust is called a root certificate
    \begin{itemize}
    \item A root certificate is a certificate identifying a root CA
    \end{itemize}
\item By definition, root certificates are self-signed
    \begin{itemize}
    \item In practice, they have multiple multiple trust-paths, e.g., in cases of cross-signatures
    \item They can also be checked by some form of secure physical distribution
    \end{itemize}
\item The HTTPS PKIs depend on a set of root certificates
    \begin{itemize}
    \item If some certificate is presented, we check if it is signed by a root CA
    \item If not, we check if the signatory has a certificate signed by a root CA
    \item Etc. (chain of trust)
    \end{itemize}
\end{itemize}
\end{frame}

\end{document}
