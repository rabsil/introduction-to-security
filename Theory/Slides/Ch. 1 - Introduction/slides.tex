\input{../common/header.tex}
\input{../common/cmds.tex}

\title{Ch. 1 - Introduction}

\begin{document}
\input{../common/front.tex}

\begin{frame}
\frametitle{Before we start}
\begin{itemize}[<+->]
\item Since WW2, there is a necessity to protect computer systems
	\begin{itemize}
	\item Different types of threat
	\item Different types of protection
	\end{itemize}
\end{itemize}
\begin{alertblock}<+->{Problem}
	\begin{itemize}[<+->]
	\item Strong security is heavier
	\end{itemize}
\end{alertblock}
\begin{exampleblock}<+->{Solution}
	\begin{itemize}[<+->]
	\item Analyse each risk and its probability to occur, and adapt your strategy
	\end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}
\frametitle{Types of risks}
\begin{itemize}[<+->]
\item Different types of risks
	\begin{itemize}
	\item Access a restricted resource or service
	\item Usurp somebody else's identity
	\item Access confidential data
	\item Falsification
	\end{itemize}
\item In this course, \emph{only} these risks are covered
\item For each of those risks
	\begin{itemize}
	\item Several possible attacks
	\item Several counter-measures available
	\end{itemize}
\end{itemize}
\end{frame}

\section{Objectives}

\begin{frame}
\frametitle{"When in doubt, use brute force"}
\begin{itemize}[<+->]
\item Many security systems rely on hard to solve mathematical problems
	\begin{itemize}
	\item Hard: takes a "considerable" amount of time without the proper codes
	\end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Example}
	\begin{itemize}[<+->]
	\item Big numbers factorisation
	\item Invert functions
	\item Combinatorial optimisation
	\item Stochastic optimisation
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item We're interested in this type of constraints
	\begin{itemize}
	\item Maths left aside
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Objectives}
\begin{enumerate}[<+->]
\item Availability: make sure a service is available for authorized users
\item Authentication: make sure only authorised users have access to restricted resources and services
\item Non repudiation: make sure a contract between $x$ and $y$ \emph{really} comes from $x$ and is \emph{really} being concluded with $y$
\item Confidentiality: make sure confidential data are confidential for anyone not authorised
\item Integrity: make sure data are left unaltered during transit
\end{enumerate}
\end{frame}

\section{Algorithmic complexity}

\begin{frame}
\frametitle{Motivation: security}
\begin{itemize}[<+->]
\item Often, security relies on complex mathematical problems
\end{itemize}
\begin{exampleblock}<+->{Example}
	\begin{itemize}[<+->]
	\item It must be "easy" to compute some function
	\item It must be "hard" to perform some action
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Intuition
	\begin{itemize}
	\item Easy = "fast" to compute
	\item Hard = "slow" to compute
	\end{itemize}
\item We need a notion allowing to characterise this
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Other motivation: efficiency}
\begin{itemize}[<+->]
\item Resources available during execution are limited
	\begin{itemize}
	\item Time
	\item Memory
	\end{itemize}
\item A requirement in algorithms is "efficiency"
\end{itemize}
\begin{exampleblock}<+->{Question}
	\begin{itemize}[<+->]
	\item What does it mean to be efficient?
		\begin{itemize}
		\item Time? Memory?
		\item In average? In the worst case?
		\end{itemize}
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item We need a notion allowing to characterise this
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Context}
\begin{itemize}[<+->]
\item At several locations, libraries detail algorithmic complexity
	\begin{itemize}
	\item Worst case time complexity
	\item Asymptotic behaviour
	\end{itemize}
\item At several places, we talk about "easy" or "hard" problems
\end{itemize}
\begin{exampleblock}<+->{Example}
	\begin{itemize}[<+->]
	\item "In the worst case, \texttt{max} processes a number of elements linearly proportional the size of the underlying container
	\item It is hard to decode a ciphered text without the key
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item This section is a short, non mathematical, introduction
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Counting elementary instructions}
\begin{itemize}
\onslide<1-> \item Characterising algorithmic complexity requires a counting "elementary operations" executed by a program
\onslide<2-> \item Atomic operations, running "for the same duration"
	\begin{itemize}
	\onslide<3-> \item To simplify, a \texttt{+} takes as much time as a \texttt{*}
	\end{itemize}
\onslide<4-> \item We count arithmetic and logic operations, memory accesses, assignations, etc.
\end{itemize}
\visible<5-|handout:1>{
\begin{exampleblock}{Example: maximum search algorithm}
	\begin{center}
	\includegraphics{pics/max.pdf}
	\end{center}
\end{exampleblock}}
\end{frame}

\begin{frame}
\frametitle{Detailed analysis}
\begin{itemize}[<+->]
\item Let's analyse the complexity of this algorithm
	\begin{enumerate}
	\item $2$ instruction required: one for memory access, one for assignation
	\item We need to split the iterations
		\begin{itemize}
		\item First iteration: $2$ instructions (one for \texttt{i = 0}, one for \linebreak\texttt{i < n})
		\item At each other iteration: $2$ instructions ($n-1$ times)
		\end{itemize}
	\item The \lstinline|if| statement is always considered (worst case): $2$ instructions ($n$ times)
	\item The body of the \lstinline|if| also runs with $2$ instructions ($n$ times)
	\end{enumerate}
\item Summary: this algorithm executes in the worst case $$2 + 2 + 2 \cdot (n-1) + 2n + 2n = 6n + 2$$ elementary operations
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Asymptotic behaviour}
\begin{itemize}[<+->]
\item Counting elementary operations like before is boring
	\begin{itemize}
	\item Do we really need to count this precisely?
	\item Relevence regarding the definition of an elementary operation?
	\end{itemize}
\item We are interested in asymptotic behaviour
	\begin{itemize}
	\item When $n$ is large
	\end{itemize}
\item We "drop" the $2$
	\begin{itemize}
	\item It's an "initialisation constant"
	\item In \java, we need some time to set up the VM
	\item Why consider it?
	\end{itemize}
\item We "drop" the $6$
	\begin{itemize}
	\item In \java, we have bound checking
	\end{itemize}
\item Worst case complexity: $\bigo(n)$
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{A proper definition}
\begin{exampleblock}<+->{Definition}
	\begin{itemize}[<+->]
	\item $\bigo(f(n)) = \left\{ g(n) \suchthat \exists c > 0, \exists n_0 \in \IN, \forall n \geqslant n_0, 0 \leqslant g(n) \leqslant cf(n) \right\}$.
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item There exist a point after which $g$ is lower than $f$, up to some multiplicative constant
	\begin{itemize}
	\item This definition is out of context
	\end{itemize}
\end{itemize}
\begin{exampleblock}<+->{What matters}
	\begin{itemize}[<+->]
	\item «~$f(n)$ asymptotically behaves like $g(n)$~»
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item It takes "as much time" to look for a maximum in a list than to print its content
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Complexity classes and examples}
\begin{minipage}{4.5cm}
\begin{itemize}
\onslide<2-> \item $\bigo(1)$: array element access
\onslide<3-> \item $\bigo(\log(n))$: binary search
\onslide<4-> \item $\bigo(n)$: maximum
\onslide<5-> \item $\bigo(n\log(n))$: efficient sort
\onslide<6-> \item $\bigo(n^2)$: inefficient sort
\onslide<7-> \item $\bigo(e^n)$: naive Fibonacci
\onslide<8-> \item "Easy": bounded by a polynom
\end{itemize}
\end{minipage}
\hfill
\begin{minipage}{7cm}
\includegraphics[width=7cm]{pics/bigo.pdf}
\end{minipage}
\end{frame}

\section{Main topics}

\subsection{Hash}

\begin{frame}
\frametitle{Cryptographic hash function}
\begin{itemize}[<+->]
\item Given a message of arbitrary size, provides a fixed size imprint
	\begin{itemize}
	\item Message digest
	\end{itemize}
\item Hard to revert
	\begin{itemize}
	\item $1M$\$ question
	\end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Basic properties}
	\begin{enumerate}[<+->]
	\item Message digest is easy to compute
	\item Hard to generate a message from its hash
	\item Hard to change a message without its digest
	\item Hard to find two different messages with the same digest
	\end{enumerate}
\end{exampleblock}
\begin{itemize}[<+->]
\item Applications in integrity and authentication
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Examples}
\begin{itemize}
\item Computing message digest of "These measures will increase the club's ability to generate revenue"
\end{itemize}
\begin{lstlisting}
4ad6fdf0a28fd29bf3c1161400ca2b06
\end{lstlisting}
\begin{itemize}
\item Computing message digest of "These measures will increase the club's ability to generate revenge"
\end{itemize}
\begin{lstlisting}
206db375837d0927b35de60c08492950
\end{lstlisting}
\end{frame}

\subsection{Ciphers}

\begin{frame}
\frametitle{Data encryption}
\begin{itemize}[<+->]
\item Basic idea: make confidential data unreadable for unauthorized people
\item Different from steganography
\item The messages are encoded with one or two "keys"
	\begin{enumerate}
	\item Single key: symmetric cryptography
		\begin{itemize}
		\item This key is \emph{secret}
		\end{itemize}
	\item A key to encode (public), a key to decode (private): public key (asymmetric) cryptography
		\begin{itemize}
		\item Only the private key is secret
		\end{itemize}
	\end{enumerate}
\item Without the key, it is hard to recover the original message
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Ciphered text example}
\begin{itemize}
\item AES-256 ciphering example
\item Key (openssl)
\end{itemize}
\begin{lstlisting}
salt=F00901B5CCFFDA4A
key=3AE954A6F8776405946EBD774D289A6922626996D5F81A51E5F454AC2FF85081
iv =BF8DD2EAC80EC30861C726E542465FA6
\end{lstlisting}
\begin{itemize}
\item Plain text: "This introduction is marvelous!"
\item Result (displayed in base 64)
\end{itemize}
\begin{lstlisting}
U2FsdGVkX181j/ZKofmJkY+m3JVwmlSpgLKSLtiF0/onKilO5E3nju8GHcsARia/FlhFWYccoppO78W/NvV4Rw==
\end{lstlisting}
\end{frame}

\subsection{Signature}

\begin{frame}
\frametitle{Numerical signature}
\begin{itemize}[<+->]
\item Basic idea: make sure the sender is who we think he is
\item More sophisticated than a signature at the bottom of a page
	\begin{itemize}
	\item Usurp the identity of someone must be hard
	\item Principle: we sign "through" the whole document, imprinting datas.
	\end{itemize}
\item Works with a combination of hash functions and ciphers
	\begin{itemize}
	\item A key for signature and another one for authentication
	\end{itemize}
\item Makes sure the message
	\begin{itemize}
	\item was left unaltered
	\item is genuine
	\end{itemize}
\end{itemize}
\end{frame}

\subsection{Certification}

\begin{frame}
\frametitle{Exchanging keys}
\begin{alertblock}<+->{Problem}
	\begin{itemize}
	\item How to safely exchange cryptographic keys ?
	\end{itemize}
\end{alertblock}
\begin{exampleblock}<+->{Solution}
	\begin{itemize}
	\item Use a third party intermediate that both actors trust
	\end{itemize}	
\end{exampleblock}
\begin{itemize}[<+->]
\item Certification authority (Verisign, Symantec, etc.)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Certificate}
\begin{itemize}[<+->]
\item Electronic document proving the property of a public key
\item Widely used for first-time key exchange
\item Contains
	\begin{itemize}
	\item public key information
	\item key owner information
	\item numerical signature of the certification authority
	\end{itemize}
\item The certification authority makes sure information stored in the certificate are correct, and signs it
\end{itemize}
\end{frame}

\section{Kerckhoff's principles}

\begin{frame}
\frametitle{Origin}
\begin{itemize}[<+->]
\item Introduced in 1883 by Auguste Kerckhoff, in military context
\end{itemize}
\begin{exampleblock}<+->{Principles}
	\begin{enumerate}[<+->]
	\item The system must be materially and mathematically unbreakable without the key
	\item The system must not require any secret (beyond the key), and must not be compromised should it fall into enemy hands
	\item The system must be "easy" to use
	\end{enumerate}
\end{exampleblock}
\begin{itemize}[<+->]
\item Security \emph{cannot} rely on secrecy
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Summary}
\begin{itemize}[<+->]
\item Secrecy is a weakness
	\begin{itemize}
	\item Arguments such as "your system is unsafe because we can see the source code" or "my system is safer than yours because my source code is hidden" are naive, and invalid
	\end{itemize}
\item It is hard (if not impossible) to bypass security without the proper codes
\item These codes must be easily modified
\item The security system must be analysed by experts
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Consequence}
\begin{itemize}[<+->]
\item Theses principles can be in particular applied to cryptography, the codes on which it relies being the keys
\end{itemize}
\begin{exampleblock}<+->{Summary}
	\begin{itemize}[<+->]
	\item The opponent \emph{knows} the cryptographic system
	\end{itemize}
\end{exampleblock}
\begin{alertblock}<+->{Consequence}
	\begin{itemize}[<+->]
	\item A system's security must rely on the key \emph{only}, not on the system itself
	\end{itemize}
\end{alertblock}
\end{frame}


\section{Avalanche effect}

\begin{frame}
\frametitle{That's what she said}
\begin{itemize}[<+->]
\item No algorithm can process an arbitrary large amount of data at once
    \begin{itemize}
    \item Too large to be completely buffered
    \item Large secure buffers are costy to set up
    \end{itemize}
\item The input needs to be sliced up into smaller pieces
\item Two main techniques
    \begin{enumerate}
    \item Use blocks (in cryptographic hash functions and block ciphers, cf. Ch. 2 \&\ 3)
    \item Use streams (in stream ciphers, cf. Ch. 3)
    \end{enumerate}
\item For block algorithms, we need to add padding for (usually) the last block
    \begin{itemize}
    \item If it isn't the right size
    \end{itemize}
\item This divide and conquer strategy must remain secure
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The butterfly effect}
\begin{itemize}[<+->]
\item Desired effect of cipher algorithms and cryptographic hash functions
\item Idea: if we slightly change the input, the output changes greatly
	\begin{itemize}
	\item Flipping a bit in the plain text will strongly change the ciphered text
	\item Flipping a bit in a message strongly changes its hash
	\end{itemize}
\item If this effect is not strong enough, the randomness of the output is low, and cryptanalyst can deduce clues
	\begin{itemize}
	\item related to the plain text or the private key, from the ciphered text,
	\item related to a message from its hash
	\end{itemize}
\item Also provides strong integrity when fighting attackers
\item Reduced on stream ciphers
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Avalanche effect}
\begin{itemize}[<+->]
\item Two avalanche criterions
\end{itemize}
\begin{exampleblock}<+->{Strict avalanche criterion}
	\begin{itemize}[<+->]
	\item If a single bit is flipped in the input, each bit of the output has 50\% chances to change
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item A slight change has great impact on the output
\end{itemize}
\begin{exampleblock}<+->{Bits independence criterion}
	\begin{itemize}[<+->]
	\item Two bits $i$ and $j$ of the output independently change when a bit $k$ of the input is flipped, for each $i,j,k$.
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item No dependencies between bit-flips
\end{itemize}
\end{frame}


%\subsection{Random number generation}
%
%\begin{frame}
%\frametitle{Random number generator}
%\begin{itemize}[<+->]
%\item In cryptography, we often need random numbers
%\end{itemize}
%\begin{exampleblock}<+->{Example}
%	\begin{itemize}[<+->]
%	\item Key generation
%	\item Password generation
%	\item Noise generation
%	\end{itemize}
%\end{exampleblock}
%\begin{itemize}[<+->]
%\item How to make it \emph{really} random ?
%	\begin{itemize}
%	\item Very complex in practice
%	\end{itemize}
%\item Two main techniques
%	\begin{enumerate}
%	\item Physical methods
%	\item Computational methods
%	\end{enumerate}
%\end{itemize}
%\end{frame}
%
%\begin{frame}
%\frametitle{Physical methods}
%\begin{minipage}{.65\textwidth}
%\begin{itemize}[<+->]
%\item Gather the entropy of a physical process
%	\begin{itemize}
%	\item Shot noise (poisson)
%	\item Radioactive thermic and electromagnetic radiation
%	\item Radio interference
%	\end{itemize}
%\item Implemented with white noise generator
%\item Combination of several generators
%\item Considered to be \emph{true} random numbers generators
%\end{itemize}
%\end{minipage}
%\begin{minipage}[t]{.3\textwidth}
%\includegraphics[width=.95\textwidth]{pics/Photon-noise.jpg}\\
%\includegraphics[width=.95\textwidth]{pics/s-l300.jpg}
%\end{minipage}
%\end{frame}
%
%\begin{frame}
%\frametitle{Computational methods}
%\begin{itemize}[<+->]
%\item Use a \emph{a priori} deterministic algorithm
%\item Computationally mimic a random process
%\item Concept of \emph{seed}: everything relies on the first value
%\end{itemize}
%\begin{exampleblock}<+->{Example}
%	\begin{itemize}[<+->]
%	\item $\pi$ is transcendent
%	\item Fractional digits of a transcendent are conjectured to be randomly and uniformly distributed
%	\item Generate a large portion of $\pi$ fractional part and set a seed
%	\item The first value is the seed, the second one the next digit, etc.
%	\end{itemize}
%\end{exampleblock}
%\end{frame}

%\subsection{Padding}
%
%\begin{frame}
%\frametitle{Padding}
%\begin{itemize}[<+->]
%\item No cryptographic algorithm processes all its input in one time
%\item The input is arbitrary large
%\item Use of a \emph{padding scheme}
%\item Idea: cut the input into blocks of equal size, and process each block separately
%	\begin{itemize}
%	\item Hash: iteratively input blocks to a one-way compression function (Merkle-Damgård construction)
%	\item Cipher: each block is ciphered separately (CBC)
%	\item Signature: each block is hashed and ciphered
%	\end{itemize}
%\item The size of a block is algorithm dependent
%	\begin{itemize}
%	\item Java: \texttt{doFinal} errors
%	\end{itemize}
%%\item The output of each block depends on the previously output block
%\end{itemize}
%\end{frame}

\end{document}
